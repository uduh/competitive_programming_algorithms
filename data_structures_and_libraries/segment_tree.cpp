/*
 Segment tree for range sum with lazy updates.
 Storage: O(n)
 Build: O(n)
 Query: O(\log n)
 Update: O(\log n)
 */

#include <cmath>
#include <iostream>
using namespace std;

#define MAX_SIZE 100000

// 0 indexed segment tree, both range boundaries inclusive.
class SegmentTree{
  private:
    int stree[MAX_SIZE];
    int n;
    int lazy[MAX_SIZE];

    int left(int index) {
        // Same as 2 * p + 1.
        return (index << 1) + 1;
    }

    int right(int index) {
        // Same as 2 * p + 2.
        return (index << 1) + 2;
    }

    // Should be commutative and associative, ex: +, *, max, min.
    int operation(int a, int b) {
        return a + b;
    }

    int build(const int* a, int index, int s, int e) {
        if (s == e) {
            // We are at a leaf.
            stree[index] = a[s];
            return stree[index];
        }
        else {
            int mid = (s + e) / 2;
            stree[index] = operation(build(a, left(index), s, mid), build(a, right(index), mid+1, e));
            return stree[index];
        }
    }

    int query(int index, int qs, int qe, int ss, int se) {
        // Lazy updates, done for the example of sum, should be tweaked for others.
        if (lazy[index] != 0) {
            // Determine the result of this node, should be tweaked.
            stree[index] += (se - ss + 1) * lazy[index];

            // Push lazy updates if we are not at leaves.
            if (se != ss) {
                lazy[left(index)] += lazy[index];
                lazy[right(index)] += lazy[index];
            }

            lazy[index] = 0;
        }

        // Current segment out of query range.
        if (ss > se || ss > qe || se < qs) {
            // Should be identity element, or out of bound index.
            return 0;
        }

        // Fully inside query range.
        if (qs <= ss && qe >= se) {
            return stree[index];
        }

        int mid = (ss + se) / 2;
        return operation(query(left(index), qs, qe, ss, mid), query(right(index), qs, qe, mid+1, se));
    }

    void update(int index, int diff, int us, int ue, int ss, int se) {
        // Lazy updates, done for the example of sum, should be tweaked for others.
        if (lazy[index] != 0) {
            // Determine the result of this node, should be tweaked.
            stree[index] += (se - ss + 1) * lazy[index];

            // Push lazy updates if we are not at leaves.
            if (se != ss) {
                lazy[left(index)] += lazy[index];
                lazy[right(index)] += lazy[index];
            }
            lazy[index] = 0;
        }

        // Current segment out of query range.
        if (ss > se || ss > ue || se < us) {
            return;
        }

        // Fully inside query range, with lazy updates, otherwise make this if only for leaf case.
        // Done for the example of sum, should be tweaked for others.
        if (us <= ss && ue >= se) {
            // Determine the result of this node, should be tweaked.
            stree[index] += (se - ss + 1) * diff;

            // Push lazy updates if we are not at leaves.
            if (se != ss) {
                lazy[left(index)] += diff;
                lazy[right(index)] += diff;
            }

            return;
        }

        int mid = (ss + se) / 2;
        update(left(index), diff, us, ue, ss, mid);
        update(right(index), diff, us, ue, mid+1, se);
        stree[index] = operation(stree[left(index)], stree[right(index)]);
    }

public:
    SegmentTree(const int* a, int n) {
        this->n = n;
        // Max size should be at least pow(2, ceil(log2(n) + 1)) - 1 or around 4n.
        for (int i = 0; i < 4 * n; i++) {
            lazy[i] = 0;
        }
        build(a, 0, 0, n - 1);
    }

    int query(int qs, int qe) {
        return query(0, qs, qe, 0, n - 1);\
    }

    void update(int diff, int us, int ue) {
        update(0, diff, us, ue, 0, n - 1);
    }
};


int main() {
    // Test 1
    int n = 7;
    int a[] = {1, 2, 3, 4, 5, 6, 7};
    SegmentTree st1(a, n);

    cout << st1.query(0, 6) << endl;  // 28
    st1.update(1, 0, 6);
    cout << st1.query(0, 6) << endl;  // 35
    st1.update(1, 0, 0);
    cout << st1.query(0, 6) << endl << st1.query(1, 6) << endl << endl;  // 36, 33

    // Test 2
    int n2 = 7;
    int a2[] = {18, 17, 13, 19, 15, 11, 20};
    SegmentTree st2(a2, n2);
    cout << st2.query(1, 3) << endl << st2.query(4, 6) << endl;  // 49, 46

    return 0;
}
