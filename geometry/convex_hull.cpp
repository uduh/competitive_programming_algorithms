/*
 Graham's Scan for Convex Hull
 O(n \log n)
 Uses point, distance, areCollinear() - (vec, crossProduct()), ccw()
*/

#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include "basic_geometry.hpp"
using namespace std;

point pivot(0, 0);

bool angleCompare(point a, point b) {
    if (areCollinear(pivot, a, b)) {
        return distance(pivot, a) < distance(pivot, b);
    }

    double d1x = a.x - pivot.x, d1y = a.y - pivot.y;
    double d2x = b.x - pivot.x, d2y = b.y - pivot.y;
    return (atan2(d1y, d1x) - atan2(d2y, d2x)) < 0;
}

vector<point> convexHull(vector<point> P) {
    // Trivial case.
    if (P.size() <= 3) {
        if (!(P[0] == P[P.size() - 1])) {
            P.push_back(P[0]);
        }
        return P;
    }

    // Setup, find pivot.
    int P0 = 0;  // lowest y, rightmost x
    for (int i = 0; i < P.size(); i++) {
        if (P[i].y < P[P0].y || (P[i].y == P[P0].y && P[i].x > P[P0].x)) {
            P0 = i;
        }
    }

    point temp = P[0];
    P[0] = P[P0];
    P[P0] = temp;

    // Sort by angle.
    pivot = P[0];
    sort(++P.begin(), P.end(), angleCompare);

    // Algorithm.
    vector<point> S;
    S.push_back(P[P.size()-1]);
    S.push_back(P[0]);
    S.push_back(P[1]);

    for (int i = 2; i < P.size();) {
        if (ccw(S[S.size() - 2], S[S.size() - 1], P[i])) {
            S.push_back(P[i++]);
        }
        else {
            S.pop_back();
        }
    }

    return S;
}

int main() {
    // Sample input.
    int n, x, y;
    cin >> n;
    vector<point> P;

    for (int i = 0; i < n; i++) {
        cin >> x >> y;
        P.push_back(point(x, y));
    }

    vector<point> S = convexHull(P);

    for (int i = 0; i < S.size(); i++) {
        cout << S[i].x << " " << S[i].y << endl;
    }

    return 0;
}
/*
 test case 1: 72 0 0 1 2 -2 1 -1 -1 3 4 4 3 -5 4 6 5 7 7 7 -7 -7 -7 -7 7 9 0 -9 0 0 9 0 -9 -8 0 8 0 -7 0 7 0 -6 0 6 0 -5 0 5 0 -4 0 4 0 -3 0 3 0 -2 0 2 0 -1 0 1 0 0 -8 0 8 0 -7 0 7 0 -6 0 6 0 -5 0 5 0 -4 0 4 0 -3 0 3 0 -2 0 2 0 -1 0 1 1 1 2 2 3 3 4 4 5 5 6 6 1 -1 2 -2 3 -3 4 -4 5 -5 6 -6 -1 1 -2 2 -3 3 -4 4 -5 5 -6 6 -1 -1 -2 -2 -3 -3 -4 -4 -5 -5 -6 -6
 test case 2: 16 7 7 7 -7 -7 -7 -7 7 9 0 -9 0 0 9 0 -9 0 0 1 2 -2 1 -1 -1 3 4 4 3 -5 4 6 5
 output (same for both):
 -7 -7
 0 -9
 7 -7
 9 0
 7 7
 0 9
 -7 7
 -9 0
 -7 -7
*/
