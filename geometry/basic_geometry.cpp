/*
 Basic Geometry Structures and Algorithms
*/

#include <iostream>
#include <cmath>
#include "basic_geometry.hpp"
using namespace std;

/// Point functions.

double distance(point p1, point p2) {
    return sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

point rotate(point p, double theta) {  // Theta in rad, rotates around the origin.
    return point(p.x * cos(theta) - p.y * sin(theta), p.x * sin(theta) + p.y * cos(theta));
}

/// Line functions

void pointToLine(point p1, point p2, line &l) {
    if (p1.x == p2.x) {
        l.a = 1;
        l.b = 0;
        l.c = -p1.x;
    }
    else {
        l.a = -(double)(p1.y - p2.y) / (p1.x - p2.x);
        l.b = 1;
        l.c = -(double)(l.a * p1.x) - (double)p1.y;
    }
}

bool areParallel(line l1, line l2) {
    return l1.a == l2.a && l1.b == l2.b;
}

bool areSame(line l1, line l2) {
    return areParallel(l1, l2) && l1.c == l2.c;
}

bool intersect(line l1, line l2, point &p) {
    if (areParallel(l1, l2)) return false;
    p.x = (l2.b * l1.c - l1.b * l2.c) / (l2.a * l1.b - l1.a * l2.b);

    if (l1.b != 0) {
        p.y = -(l1.a * p.x + l1.c);
    }
    else {
        p.y = -(l2.a * p.x + l2.c);
    }
    return true;
}


/// Vec functions.

vec pointToVec(point p1, point p2) {  // vector p1->p2
    return vec(p2.x - p1.x, p2.y - p1.y);
}

vec scale(vec v, double s) {  // v*s
    return vec(v.x*s, v.y*s);
}

point translate(point p, vec v) {  // Translate p by v.
    return point(p.x + v.x, p.y + v.y);
}

/// Dot product.

double dotProduct(vec a, vec b) {
    return (a.x * b.x + a.y * b.y);
}

double distanceToLine(point p, point a, point b, point &c) {  // Dist from p to line ab.
    //c = a + u * ab
    vec ap = pointToVec(a, p), ab = pointToVec(a, b);
    double u = dotProduct(ap, ab) / dotProduct(ab, ab);
    c = translate(a, scale(ab, u));
    return distance(p, c);
}

double distanceToLineSegment(point p, point a, point b, point &c) {  // Dist from p to line segment ab.
    vec ap = pointToVec(a, p), ab = pointToVec(a, b);
    double u = dotProduct(ap, ab) / dotProduct(ab, ab);
    if (u < 0.0) {
        c = point(a.x, a.y);
        return distance(p, a);
    }
    if (u > 1.0) {
        c = point(b.x, b.y);
        return distance(p, b);
    }
    return distanceToLine(p, a, b, c);
}

double angle(point a, point o, point b) {  // Size of angle aob.
    vec oa = pointToVec(o, a), ob = pointToVec(o, b);
    return acos(dotProduct(oa, ob) / sqrt(dotProduct(oa, oa) *dotProduct(ob, ob)));
}


/// Cross product.

double crossProduct(vec a, vec b) {  // Only absolute value.
    return a.x * b.y - a.y * b.x;
}

bool ccw(point p, point q, point r) {  // True if r is to the right of line pq.
    return crossProduct(pointToVec(p, q), pointToVec(p, r)) > 0;
}

bool areCollinear(point p, point q, point r) {
    return crossProduct(pointToVec(p, q), pointToVec(p, r)) == 0;
    // Rather use (precision errors):
    // Return fabs(crossProduct(pointToVec(p, q), pointToVec(p, r))) < 1e-9;
}

// Circles: (x - a)^2 + (y - b)^2 = r^2, c = 2 * PI * r, A = PI * r^2
// Chord length = 2*r*sin(theta/2)

int insideCircle(point p, point c, int r) {  // Is p inside the circle.
    int dx = p.x - c.x, dy = p.y - c.y;
    int distFromCenter = dx * dx + dy * dy, rSquared = r * r;
    return distFromCenter < rSquared ? 0 : distFromCenter == rSquared ? 1 : 2;  // inside/border/outside
}

// Triangles: A = a * v_a / 2, d = a + b + c, s = d / 2
// A = sqrt(s * (s - a) * (s - b) * (s - c)) - Heron's formula
// Inscribed circle radius r = A / s
// Circumscribed circle radius R = a * b * c / (4 * A)
// c^2 = a^2 + b^2 - 2 * a * b * cos(gama) - Law of Cosines
// a / sin(\alpha) = b / sin(\beta) = c / sin(\gamma) = 2 * R - Law of Sines

double triangleCir(double a, double b, double c) {
    return a + b + c;
}

double triangleArea(double a, double b, double c) {
    double s = triangleCir(a, b, c) / 2;
    return sqrt(s * (s - (double)a) * (s - (double)b) * (s - (double)c));
}

double rInscribedCircle(point a, point b, point c) {
    double ab = distance(a, b), bc = distance(b, c), ca = distance(c, a);
    return triangleArea(ab, bc, ca) / (0.5 * triangleCir(ab, bc, ca));
}

bool inscribedCircle(point p1, point p2, point p3, point &center, double &r) {
    r = rInscribedCircle(p1, p2, p3);
    if (r == 0) return 0;  // No inscribed circle.
    // Better: if (fabs(r) < 1e-9) return 0;

    line l1, l2;
    double ratio = distance(p1, p2) / distance(p1, p3);
    point p = translate(p2, scale(pointToVec(p2, p3), ratio / (1 + ratio)));
    pointToLine(p1, p, l1);
    ratio = distance(p2, p1) / distance(p2, p3);
    p = translate(p1, scale(pointToVec(p1, p3), ratio / (1 + ratio)));
    pointToLine(p2, p, l2);
    intersect(l1, l2, center);
    return 1;
}

double rCircumscribedCircle(point a, point b, point c) {
    double ab = distance(a, b), bc = distance(b, c), ca = distance(c, a);
    return (ab * bc * ca) / (4.0 * triangleArea(ab, bc, ca));
}

int main_bg() {
    point a = point(0, 0), b = point(0, 3), c = point(4, 0);
    int ab = distance(a, b), bc = distance(b, c), ca = distance(c, a);
    cout << triangleCir(ab, bc, ca) << endl << triangleArea(ab, bc, ca) << endl << rInscribedCircle(a, b, c) << endl << rCircumscribedCircle(a, b, c) << endl;
    //12, 6, 1, 2.5
    return 0;
}
