#ifndef basic_geometry_h
#define basic_geometry_h

struct point {
    int x, y; // If double, don't check for equality, but rather fabs(x-y) < 1e-9.
    point() {
        x = 0;
        y = 0;
    }

    point (int _x, int _y): x(_x), y(_y) {
    }

    bool operator<(point other) const {
        if (x != other.x) {
            return x < other.x;
        }
        return y < other.y;
    }

    bool operator==(point other) const {
        return (x == other.x) && (y == other.y);
    }
};

struct line {  // ax + by + c = 0
    double a, b, c;
};

struct vec {  // 2D vector.
    double x, y;
    vec(double _x, double _y): x(_x), y(_y) {};
};

double distance(point p1, point p2);

bool ccw(point p1, point p2, point p3);

double angle(point a, point o, point b);

vec pointToVec(point p, point q);

double crossProduct(vec a, vec b);

//for convex hull

bool areCollinear(point p, point q, point r);

#endif /* Basic_geometry_h */
