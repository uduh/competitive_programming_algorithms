/*
 Floyd Warshall's Algorithm for All-Pairs Shortest Paths (APSP)
 O(V^3)
 For minimax (see Kruskal's) change update to distance[i][j] = min(distance[i][j], max(distance[i][k], distance[k][j]));
 distance[i][i] represents the length of the cycle that goes through i. Negative if the cycle is negative.
 The diameter of the graph (longest path from some i to some j) is max in table distance
 Can also find SCC. If distance[i][j] < INF && distance[j][i] < INF, then j and i are part of the same SCC. Speed up by only storing bools in distance.
*/

#include <iostream>
using namespace std;

const int INF = 500000;

int main() {
    // Sample input.
    int n, m, x, y, w = 0;
    cin >> n >> m;
    int adj[n][n];  // Adjacency Matrix.

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            adj[i][j] = INF;

    for (int i = 0; i < m; i++) {
        cin >> x >> y >> w;
        adj[x][y] = w;
    }

    // Algorithm.""
    int distance[n][n];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            distance[i][j] = adj[i][j];

    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                distance[i][j] = min(distance[i][j], distance[i][k] + distance[k][j]);
            }
        }
    }
    
    //to print the shortest path, save parents:
    //init: p[i][j] = i for all i and j
    //if distance[i][j] update is sucesssful, p[i][j] = p[k][j]
    //print SSSP by:
    //void printPath(int i, int j) {
    //    if (i != j) printPath(i, p[i][j]);
    //    printf(" %d", j);
    //}
    return 0;
}
