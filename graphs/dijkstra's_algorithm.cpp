/*
 Dijkstra's Algorithm for Single Source Shortest Path (SSSP) Without Negative Cycles
 Works if weights are negative, but slower.
 O((E + V)v\log V)
 Very useful for state-space search (see complete search), some smaller graphs can be remodelled to state-space graphs.
*/

#include <iostream>
#include <list>
#include <queue>
#include <vector>
using namespace std;

#define MAX_SIZE 100000

list<pair<int, int> > adj[MAX_SIZE];
const int INF = 5000000;

int main() {
    // Sample input.
    int n, m, x, y, w = 0;
    cin >> n >> m;
    // Test case: 5 7 0 4 1 1 3 3 1 4 6 2 1 2 2 3 7 2 0 6 3 4 5 Output: 6 2 0 5 7
    // adj is (second vertex, weight)

    for (int i = 0; i < m; i++) {
        cin >> x >> y >> w;
        adj[x].push_back(make_pair(y, w));
    }

    // Algorithm.
    int source = 2;
    int distance[n];
    for (int i = 0; i < n; i++) distance[i] = INF;
    distance[source] = 0;

    priority_queue<pair<int, int>, vector<pair<int, int> >, greater<pair<int,int> > > pq;
    pq.push(make_pair(0, source));  // length, vertex

    while (!pq.empty()) {
        pair<int, int> front = pq.top();
        pq.pop();
        // Because of lazy deletion, we do not delete duplicate vertices in pq.
        if (front.first > distance[front.second]) continue;

        for (const pair<int, int>& neigh: adj[front.second]) {
            // Relax vertex and enqueue if new distance is lower.
            if (distance[front.second] + neigh.second < distance[neigh.first]) {
                distance[neigh.first] = distance[front.second] + neigh.second;
                pq.push(make_pair(distance[neigh.first], neigh.first));
            }
        }
    }

    for (int i = 0; i < n; i++)
        cout << distance[i] << " ";
    cout << endl;
    return 0;
}
