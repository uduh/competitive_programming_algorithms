/*
 Edmonds-Karp Algorithm for Max Flow / Min Cut
 O(VE^2)
*/

#include <iostream>
#include <queue>
using namespace std;

int main() {
	int n;
	cin >> n;
	int adj[n][n];

	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			cin >> adj[i][j];

	int maxFlow = 0, s = 0, t = n - 1;

	while (true) {
		vector<int> prev(n, -2);  // Visited array and predecesor.
		queue<int> q; q.push(s);

		while (!q.empty()) {  // BFS
			int u = q.front(); q.pop();
			if (u == t) break;  // Break when reaching sink.

			for (int v = 0; v < n; v++) {
				if (adj[u][v] > 0 && prev[v] == -2) {
					prev[v] = u;
					q.push(v);
				}
			}
		}
		if (prev[t] == -2) break;  // No augmenting path, maxflow found.

		int f = 5000000;
		for (int u = t; u != s; u = prev[u]) f = min(adj[prev[u]][u], f);
		for (int u = t; u != s; u = prev[u]) {
			adj[prev[u]][u] -= f;
			adj[u][prev[u]] += f;
		}
		maxFlow += f;
	}

	cout << maxFlow << endl;
}
