/*
 Typical ideas for special graphs.
*/

/*
 Directed Acyclic Graph (DAG)
 - suitable for DP, since topological ordering exists. for bottom-up solve vertices (subproblems) in the topological order, since it guarantees that all previous subproblems will be solved before solving some subproblem. for top-down start with topologically last vertex (destination vertex) and backtrack in the opposite way of the direction of the edges
 - SSSP or SSLP can be solved with DP, just relax in the topological order. use negative weights for SSLP, O(E + V)
 - counting the number of paths can be solved with DP, just add the number of paths in topological order (num[neighbour] += num[current] for all neighbours for all currents in topological order), O(E + V)
 - sometimes a non-DAG can be modelled as DAG by adding some other non-increasing (or non-decreasing) states to each vertex. typical example is time. can be also viewed as DP with (vertex, non-increasing parameter) as parameters.
 - actually, a lot of DP tasks can be viewed as SSSP/SSLP or counting the number of paths on a DAG with parameters representing vertices. sometimes it is easier to model (think about) DP tasks in this way
*/

/*
 Tree (E = V - 1, connected, acyclic, one unique path between any two vertices)
 - traversal is simpler, just visit the children without the need for a queue (pre-order (curr, left, right), in-order (left, curr, right), post-order (left, right, curr)). pre-order traversal also solves SSSP on weighted tree, O(V)
 - all edges are bridges and all vertices are articulation points, O(V)
 - APSP is simpler since running SSSP from every vertex takes, O(V^2)
 - to find a diameter of the graph first run SSSP from any vertex and find the furthest vertex. then run SSSP from that vertex, the maximum value is the diameter
*/

/*
 Eulerian Graph (has an Euler path - a path that visits each edge exactly once)
 - undirected graph is Eulerian if all except for two vertices have even degrees (even number of edges). for Euler cycle (tour) all vertices must have even degree
 - print Euler tour recoursively with the function below, O(V + E)
*/
#include <list>
#include <vector>
using namespace std;

#define MAX_SIZE 10000

list<int> cyc;
vector<pair<int, int> > adj[MAX_SIZE];

void EulerTour(list<int>::iterator i, int u) {  // Start with cyc.begin(), and starting vertex.
    for (int j = 0; j < (int)adj[u].size(); j++) {
        pair<int, int> v = adj[u][j];
        if (v.second) {  // If this edge can still be used/not removed
            v.second = 0;  // make the weight of this edge to be 0 (‘removed’).
            for (int k = 0; k < (int)adj[v.first].size(); k++) {
                pair<int, int> uu = adj[v.first][k];  // Remove bi-directional edge.
                if (uu.first == u && uu.second) {
                    uu.second = 0;
                    break;
                }
            }
            EulerTour(cyc.insert(i, u), v.first);
        }
    }
}

/*
 Bipartite graph (vertices can be assigned to two sets, all edges connect vertices from different sets)
 - check if bipartite with simple BFS or DFS coloring
 - maximum cardinality bipartite matching (MCBM) can be done with maximum flow. create a dummy vertex start that connectes to all vertices from one set and a dummy vertex finish that connects to all vertices from the other set and calculate the maximum flow.
*/
