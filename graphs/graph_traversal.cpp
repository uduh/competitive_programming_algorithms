/*
 BFS and DFS
 When using on implicit graphs (such as 2x2 grids), instead of storing neighbours, do:
 int dr[] = {1,1,0,-1,-1,-1, 0, 1};
 int dc[] = {0,1,1, 1, 0,-1,-1,-1};
 now all eight neighbours of (x, y) can be visited by (x + dr[i], y + dc[i]) for all i.
 Or construct a utility function to return neighbours.
*/

#include <iostream>
#include <list>
#include <queue>
using namespace std;

#define MAX_SIZE 100000

list<pair<int, int> > adj[MAX_SIZE];  // Adjacency list.
int DFS_visited[MAX_SIZE];
int DFS_parent[MAX_SIZE];
int BFS_distance[MAX_SIZE];
// DFS colours and BFS flags.
const int UNVISITED = 0, EXPLORED = 1, VISITED = 2;
const int INF = 5000000;

// O(V + E)
void DFS_simple_visit(int index) {
    // Set to 2, vertices with number 2 have already been visited.
    DFS_visited[index] = VISITED;

    for (const pair<int, int>& neigh : adj[index]) {
        // Vertices with number 0 are unvisited.
        if (DFS_visited[neigh.first] == UNVISITED) {
            DFS_simple_visit(neigh.first);
        }
    }

    // For topological sort, append to the start of the list here
    // and run DFS for all unvisited i from 0 to n.
    return;
}

/*
 Stable topological sort:
 enqueue vertices with zero incoming degree into a (priority) queue Q;
 while (Q is not empty) {
    vertex u = Q.dequeue(); put vertex u into a topological sort list;
    remove this vertex u and all outgoing edges from this vertex;
    if such removal causes vertex v to have zero incoming degree
        Q.enqueue(v); }
 */

// O(V + E)
void DFS_determine_edges(int index) {
    // Set to 1, vertices with number 1 are currently being explored.
    DFS_visited[index] = EXPLORED;

    for (const pair<int, int>& neigh : adj[index]) {
        // This is a normal (tree) edge.
        if (DFS_visited[neigh.first] == UNVISITED) {
            DFS_parent[neigh.first] = index;
            DFS_determine_edges(neigh.first);
        }
        else if (DFS_visited[neigh.first] == EXPLORED) {
            if (neigh.first == DFS_parent[index]) {
                printf("Two way edge from %d to %d\n", index, neigh.first);
            }
            else {
                printf("Back edge from %d to %d. Cycle detected.\n", index, neigh.first);
            }
        }
        else if (DFS_visited[neigh.first] == VISITED) {
            printf("Forward (cross) edge from %d to %d.\n", index, neigh.first);
        }
    }

    // This vertex is now visited.
    DFS_visited[index] = VISITED;
}

// O(V + E)
void BFS(int source) {
    queue<int> q;
    q.push(source);
    BFS_distance[source] = 0;

    while (!q.empty()) {
        for (const pair<int, int> neigh : adj[q.front()]) {
            // If the vertex is unvisited, calculate the distance and enqueue it.
            if (BFS_distance[neigh.first] == INF) {
                BFS_distance[neigh.first] = BFS_distance[q.front()] + 1;
                q.push(neigh.first);
            }
            // If checking for bipartite graph, colour all unvisited neighbours with the opposite colour.
            // If a visited neighbour has the same colour, the graph is not bipartite.
        }
        q.pop();
    }
}

int main() {
    // Sample input.
    int n, m, x, y, w = 0;
    cin >> n >> m;
    // adj is (second vertex, weight)

    for (int i = 0; i < m; i++) {
        cin >> x >> y/* >> w*/;
        x--; y--;
        adj[x].push_back(make_pair(y, w));
    }

    // DFS init
    for (int i = 0; i < n; i++) {
        DFS_visited[i] = UNVISITED;
    }

    // BFS init
    for (int i = 0; i < n; i++) {
        BFS_distance[i] = INF;
    }


    // Test (4 4 1 2 1 4 2 3 3 4 - YES)
    BFS(0);
    DFS_simple_visit(0);

    for (int i = 0; i < n; i++) {
        if (DFS_visited[i] == 0 && BFS_distance[i] == INF) {
            cout << "NO" << endl;
            return 0;
        }
    }
    cout << "YES" << endl;

    // Edge property test - Forward (cross) edge from 0 to 3.
    for (int i = 0; i < n; i++) {
        DFS_visited[i] = UNVISITED;
        DFS_parent[i] = -1;
    }

    DFS_determine_edges(0);
    return 0;
}
