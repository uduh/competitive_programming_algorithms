/*
 Kruskal's Algorithm for Minimum Spanning Tree (MST)
 O(E \log V)
 Uses disjoint set class.

 If a maximum spanning tree is needed, sort from greatest to lowest.
 If some edges have to be included, just unify their UF sets before running Kruskal's algorithm.
 If a minimum spanning forest of k trees is needed, stop running Kruskal's algorithm when there are k distinct UF sets.
 If the second best minimum spanning tree is needed, first find the best spanning tree and then turn off one edge in the minimum spanning tree and find the new minimum spanning tree. Repeat for each edge in the minimum spanning tree and take the minimum of the new spanning trees. Note: edges do not have to be resorted when finding new spanning trees -> O(EV)
 When finding a minimax path from i to j (path that has the smallest maximum edge weight), this path must lie on the minimum spanning tree. Maximin path similarly lies on maximum spanning tree. Also consider Floyd-Warshall for this task.
 */

#include <iostream>
#include <list>
#include <algorithm>
#include <vector>
#include "../data_structures_and_libraries/disjoint_set.cpp"
using namespace std;

vector<pair<int, pair<int, int> > > edg;  // Edge List (weight, vertex1, vertex2).

int main() {
    // Sample input.
    int n, m, x, y, w = 0;
    cin >> n >> m;

    for (int i = 0; i < m; i++) {
        cin >> x >> y >> w;
        edg.push_back(make_pair(w, make_pair(x, y)));
    }

    // Sort the edge list.
    sort(edg.begin(), edg.end());
    int cost = 0;
    UnionFind UF(n);

    // Greedily add smallest edges that do not form a cycle to the tree.
    for (int i = 0; i < m; i++) {
        if (!UF.isSameSet(edg[i].second.first, edg[i].second.second)) {
            cost+= edg[i].first;
            UF.uni(edg[i].second.first, edg[i].second.second);
        }
    }

    cout << cost << endl;
    return 0;
}
// Test case same as Prim's
