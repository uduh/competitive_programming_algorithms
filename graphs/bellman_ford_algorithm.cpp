/*
 Bellman Ford's Algorithm for Single Source Shortest Path (SSSP) Without Negative Cycles
 O(V E)
*/

#include <iostream>
#include <list>
using namespace std;

#define MAX_SIZE 10000

list<pair<int, int> > adj[MAX_SIZE];
const int INF = 5000000;

int main() {
    // Sample input.
    int n, m, x, y, w = 0;
    cin >> n >> m;
    // Test case same as Dijkstra
    // adj is (second vertex, weight)
    for (int i = 0; i < m; i++) {
        cin >> x >> y >> w;
        adj[x].push_back(make_pair(y, w));
    }

    // Algorithm.
    int source = 2;
    int distance[n];
    for (int i = 0; i < n; i++) distance[i] = INF;
    distance[source] = 0;

    for (int i = 0; i < n-1; i++) {
        for (int j = 0; j < n; j++) {
            for (const pair<int, int>& neigh : adj[j]) {
                distance[neigh.first] = min(distance[neigh.first], distance[j] + neigh.second);
            }
        }
    }

    // Negative Cycle Check.
    for (int i = 0; i < n; i++) {
        for (const pair<int, int>& neigh : adj[i]) {
            if (distance[neigh.first] > distance[i] + neigh.second) {
                cout << "Negative Cycle found!";
                return 0;
            }
        }
    }

    // Output.
    for (int i = 0; i < n; i++)
        cout << distance[i] << " ";
    cout << endl;
    return 0;
}
