/*
 Knuth-Morris-Pratt's String Matching Algorithm
 O(n + m)
*/

#include <iostream>
using namespace std;

int main() {
    string T, P;  // T = text, P = pattern
    getline(cin, T);
    getline(cin, P);
    int n = (int)T.length(), m = (int)P.length();

    // Preprocess.
    int b[m + 1];  // b = back table
    b[0] = -1;
    int i = 0, j = -1;

    while (i < m) {
        while (j >= 0 && P[i] != P[j]) j = b[j];
        i++;
        j++;
        b[i] = j;
    }
    // for (int i = 0; i < m; i++) cout<<b[i]<<" ";


    // Search.
    i = 0;
    j = 0;

    while (i < n) {
        while (j >= 0 && T[i] != P[j]) j = b[j];
        i++;
        j++;
        if (j == m) {
            cout << i - j << endl;  // Match starting at i-j.
            j = b[j];
        }
    }

    return 0;
}
/*
 I DO NOT LIKE SEVENTY SEV BUT SEVENTY SEVENTY SEVEN
 SEVENTY SEVEN
 output: 30 38
*/
