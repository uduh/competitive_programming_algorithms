/*
 Useful non-trivial complete search techniques
 TIPS, ask yourself:
 - which search branches can be pruned (simpler in recursive algorithms)?
 - how can pruning be more efficient by using some mathematical relationship? or storing something? possibly using a bitset/bitwise operations? are prunable conditions related to eachother? (N-queen example)
 - can the search depth be limited? (when we know that length < x for example) maybe iteratively deepened?
 - can any symmetries be utilized?
 - can something be precomputed?
 - can the problem be solved backwards? (rats and poison bombs example)
 - can the problem be modelled using a graph with connected states (state-space search)? is the bidirectional search faster (meet in the midle - when graphs are dense but have low height)? (sorting paragraphs example)
 - can some heuristic be defined to approximate the viability of a state/branch?
*/

#include <stdio.h>
#include <iostream>
#include <algorithm>
using namespace std;

// Output all permutations of an array.
void all_permutations(int* a, int n) {
    sort(a, a + n);
    do {
        for (int i = 0; i < n; i++) {
            cout << a[i] << " ";
        }
        cout << endl;
    } while (next_permutation(a, a + n));
}

// Output all subsets of an array.
void all_subsets(int* a, int n) {
    for (int i = 0; i < (1 << n); i++) {
        for (int j = 0; j < n; j++) {
            if (i & (1 << j)) {
                cout << a[j] << " ";
            }
        }
        cout << endl;
    }
}

int main() {
    int a[] = {2, 3, 1};
    all_permutations(a, 3);
    cout << endl;
    all_subsets(a, 3);

    return 0;
}
