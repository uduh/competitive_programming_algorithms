/*
 Some standard greedy and divide & conquer algorithms.
*/
#include <stdio.h>
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;

double fn(double x) {
    return x*x - 1.54;
}

// Function bisection - binary search for funtion roots - D&C.
double bisection(double start, double finish) {
    double err = 1e-9;
    double low = start, high = finish, mid = 0.0;

    while (fabs(high - low) > err) {
        mid = (high + low) / 2;
        if (fn(mid) >= 0 ) {
            high = mid;
        }
        else {
            low = mid;
        }
    }

    return high;
}

// Load balancing - balance n < 2m cargo in m containers, minimise imbalance - greedy.
int balance(int m, int n, vector<int> weights) {
    // Calculate average.
    int avg = 0;
    for (int i = 0; i < n; i++) {
        avg += weights[i];
    }
    avg /= m;

    // Create dummy weights.
    while (weights.size() < 2 * m) {
        weights.push_back(0);
    }

    sort(weights.begin(), weights.end());

    // Pair first with last etc.
    int imbalance = 0;
    for (int i = 0; i < m; i++) {
        imbalance += abs(weights[i] + weights[2 * m - 1 - i] - avg);
    }

    return imbalance;
}

// Interval covering problem - select as little intervals as possible to cover the whole range - greedy.
bool comp(pair<int, int> &interval1, pair<int, int> &interval2) {
    return interval1.first != interval2.first ? interval1.first < interval2.first : interval1.second > interval2.second;
}

int cover(int n, int* starts, int* ends) {
    pair<int, int> intervals[n];
    for (int i = 0; i < n; i++) {
        intervals[i].first = starts[i];
        intervals[i].second = ends[i];
    }

    // Sort by ascending start point and descending endpoint.
    sort(intervals, intervals + n, comp);

    // Take the leftmost interval that stretrches as far as possible to start with.
    int left = intervals[0].second, count = 1, max_curr = 0;

    // Iterate and take the intervals that stretch as far as possible while their starts being less than left.
    for (int i = 1; i < n; i++) {
        if (intervals[i].first <= left) {
            // Mark current one as the best choice currently.
            max_curr = max(intervals[i].second, max_curr);
        }
        else {
            // We have to choose the previous best choice and then check this interval again.
            left = max_curr;
            count++;
            i--;
        }
    }
    // Choose the last one.
    if (max_curr > left) count++;

    return count;
}

// n dragons and m knights, knight can chop dragons head off if his h >= dragon's d, determine max num of dragons killed - greedy
int dAndK(int n, int m, int* dragons, int* knights) {
    sort(dragons, dragons + n);
    sort(knights, knights + m);

    int count = 0, d = 0, k = 0;

    // d is the current dragon, k is the current knight
    while (d < n && k < m) {
        // Find a suitable knight.
        while (dragons[d] > knights[k] && k < m) k++;
        if (k == m) break;  // No more knights.
        count++;
        d++; k++;
    }

    return count;
}


int main() {
    // Tests
    cout << bisection(-60000, 60000) << endl << endl;  // 1.24097

    vector<int> v({5, 1, 2, 7});
    cout << balance(3, 4, v) << endl << endl;  // 4

    int s[] = {1, 3, 1, 2, 4, 3, 2, 7, 4, 1};
    int e[] = {2, 5, 5, 4, 5, 6, 7, 9, 8, 3};
    cout << cover(10, s, e) << endl << endl;  // 3

    int k1[] = {7, 8, 4};
    int d1[] = {5, 4};
    int k2[] = {10};
    int d2[] = {5, 5};
    cout << dAndK(2, 3, d1, k1) << endl << dAndK(2, 1, d2, k2) << endl;  // 2, 1

    return 0;
}
